#include "loft.h"

#include <cairo/cairo-xlib.h>
#include <X11/Xutil.h>

extern void _widget_update_clip (LoftWidget *w); // -- widget.c

void _arrange (LoftWidget *w) {
  if(w->visible == false)
    return;

  if(w->can_clip && w->dirty_clip)
    _widget_update_clip(w);

  loft_emit(&w->obj, "arrange", NULL);

  for(w = w->child; w != NULL; w = w->next)
    _arrange(w);
}

void _redraw (LoftWidget *w) {
  cairo_t *cr = cairo_create(w->buffer);
  cairo_set_operator(cr, CAIRO_OPERATOR_CLEAR);
  cairo_rectangle(cr, 0, 0, w->width, w->height);
  cairo_fill(cr);

  if(w->drawable) {
    cairo_set_operator(cr, CAIRO_OPERATOR_OVER);
    loft_emit(&w->obj, "draw", cr);
  }

  cairo_destroy(cr);
}

void _repaint_all (LoftWidget *w, cairo_t *cr) {
  if(w->visible == false)
    return;

  if(w->dirty)
    _redraw(w);

  cairo_set_source_surface(cr, w->buffer, w->x, w->y);
  cairo_rectangle(cr, w->x, w->y, w->width, w->height);
  cairo_fill(cr);

  w->redraw = false;

  cairo_save(cr);

  if(w->can_clip) {
    struct xywh *clip = &w->clip;
    cairo_rectangle(cr, clip->x, clip->y, clip->width, clip->height);
    cairo_clip(cr);
  }

  for(w = w->child; w != NULL; w = w->next)
    _repaint_all(w, cr);

  cairo_restore(cr);
}

void _repaint_children (LoftWidget *w, LoftWidget *n, cairo_t *cr) {
  if(w->visible == false)
    return;

  if(w->dirty)
    _redraw(w);

  cairo_set_source_surface(cr, w->buffer, w->x, w->y);
  cairo_rectangle(cr, w->x, w->y, w->width, w->height);
  cairo_fill(cr);

  w->redraw = false;

  cairo_save(cr);

  if(w->can_clip) {
    struct xywh *clip = &w->clip;
    cairo_rectangle(cr, clip->x, clip->y, clip->width, clip->height);
    cairo_clip(cr);
  }

  for(w = w->child; w != NULL; w = w->next)
    _repaint_children(w, n, cr);

  cairo_restore(cr);
}

void _repaint_stale (LoftWidget *w, LoftWidget *n, cairo_t *cr, struct xywh *clip) {
  if(w->visible == false)
    return;

  if(w->redraw) {
    int nx,ny;

    if(clip != NULL) {
      nx = clip->x;
      ny = clip->y;
    }
    else {
      nx = w->x;
      ny = w->y;
    }

    cairo_set_operator(cr, CAIRO_OPERATOR_SOURCE);
    cairo_set_source_surface(cr, n->buffer, nx, ny);
    cairo_rectangle(cr, w->x, w->y, w->width, w->height);
    cairo_fill(cr);

    cairo_set_operator(cr, CAIRO_OPERATOR_OVER);
    _repaint_children(w, n, cr);

    return;
  }

  cairo_save(cr);

  if(w->can_clip) {
    clip = &w->clip;
    cairo_rectangle(cr, clip->x, clip->y, clip->width, clip->height);
    cairo_clip(cr);
  }

  for(w = w->child; w != NULL; w = w->next)
    _repaint_stale(w, n, cr, clip);

  cairo_restore(cr);
}

void _refresh (LoftWidget *n) {
  LoftWindow *win = (LoftWindow*) n;
  cairo_t *cr = cairo_create(win->target);

  if(n->redraw) {
    if(n->dirty)
      _redraw(n);

    cairo_set_operator(cr, CAIRO_OPERATOR_SOURCE);
    cairo_set_source_surface(cr, n->buffer, 0, 0);
    cairo_rectangle(cr, 0, 0, n->width, n->height);
    cairo_fill(cr);

    if(n->child != NULL) {
      cairo_set_operator(cr, CAIRO_OPERATOR_OVER);
      _repaint_all(n->child, cr);
    }

    n->redraw = false;
  }
  else {
    cairo_set_operator(cr, CAIRO_OPERATOR_SOURCE);

    size_t i;
    for(i=0; i < win->_n_stale; ++i) {
      struct xywh *a = &win->_stale[i];
      cairo_set_source_surface(cr, n->buffer, a->x, a->y);
      cairo_rectangle(cr, a->x, a->y, a->width, a->height);
      cairo_fill(cr);
    }

    if(n->child != NULL)
      _repaint_stale(n->child, n, cr, NULL);
  }

  cairo_destroy(cr);
  win->_n_stale = 0;
}

// --

void loft_queue_refresh (LoftWidget *w, bool size_update) {
  if(loft.no_refresh)
    return;

  LoftWidget *n;
  loft_widget_native(w, &n);
  if(n == NULL)
    return;

  LoftWindow *win = (LoftWindow *) n;

  if(size_update) {
    struct wh size_req = { n->width, n->height };
    loft_emit(&n->obj, "size-request", &size_req);
  }

  XExposeEvent x = {
    .type = Expose,
    .window = win->xwin,
    .send_event = true
  };

  XSendEvent(loft.display, win->xwin, true, ExposureMask, (XEvent*) &x);
}
