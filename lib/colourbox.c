#include "loft.h"

char *COLOURBOX = "colourbox";

int _colourbox_draw (struct LoftWidget *w, cairo_t *cr, void *data) {
  LoftColourBox *c = (LoftColourBox *) w;
  rgba_cairo_set(cr, &c->colour);
  cairo_rectangle(cr, 0,0, w->width, w->height);
  cairo_fill(cr);
  return 0;
}

void loft_colourpicker_init (LoftColourBox *c) {
  loft_widget_init(&c->w, COLOURBOX, true, true, SIZE_STATIC);
  rgba_set(&c->colour, 0.0, 0.0, 0.0, 1.0);
}

void loft_colourbox_set (LoftColourBox *c, struct rgba *v) {
  rgba_copy(&c->colour, v);
}

void loft_colourbox_set_str (LoftColourBox *c, char *str) {
  rgba_from_str(&c->colour, str);
}
