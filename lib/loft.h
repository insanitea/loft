#ifndef LOFT_H
#define LOFT_H

#ifdef DEBUG
#define debug(...) fprintf(stderr, __VA_ARGS__)
#define RED "\e[0;31m"
#define GREEN "\e[0;32m"
#define YELLOW "\e[0;33m"
#define BLUE "\e[0;34m"
#define MAGENTA "\e[0;35m"
#define CYAN "\e[0;36m"
#define WHITE "\e[0;37m"
#define B_RED "\e[1;31m"
#define B_GREEN "\e[1;32m"
#define B_YELLOW "\e[1;33m"
#define B_BLUE "\e[1;34m"
#define B_MAGENTA "\e[1;35m"
#define B_CYAN "\e[1;36m"
#define B_WHITE "\e[1;37m"
#define C_ESC "\e[0m"
#else
#define debug(fmt,...)
#endif

#define MAX(A,B) (A > B ? A : B)
#define MIN(A,B) (A < B ? A : B)

#define ALLOC_POOL 4
#define ABS_MIN_SIZE 10

#include <clip/colour.h>
#include <clip/map.h>
#include <clip/list.h>
#include <pango/pangocairo.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <X11/Xlib.h>
#include <X11/keysym.h>

typedef struct LoftBuilder LoftBuilder;

typedef struct LoftEnv LoftEnv;

typedef struct LoftKey LoftKey;
typedef struct LoftButtonEvent LoftButtonEvent;
typedef struct LoftPointerEvent LoftPointerEvent;

typedef struct LoftSignalData LoftSignalData;
typedef struct LoftSignalHandler LoftSignalHandler;
typedef struct LoftTimer LoftTimer;

typedef struct LoftObject LoftObject;
typedef struct LoftAdjustment LoftAdjustment;
typedef struct LoftBuffer LoftBuffer;
typedef struct LoftBufferLine LoftBufferLine;
typedef struct LoftTextTag LoftTextTag;
typedef struct LoftTextTagData LoftTextTagData;
typedef struct LoftTextTagTable LoftTextTagTable;

typedef struct LoftWidget LoftWidget;
typedef struct LoftButton LoftButton;
typedef struct LoftColourBox LoftColourBox;
typedef struct LoftEditable LoftEditable;
typedef struct LoftLabel LoftLabel;
typedef struct LoftLayout LoftLayout;
typedef struct LoftSelectable LoftSelectable;
typedef struct LoftScrollView LoftScrollView;
typedef struct LoftScale LoftScale;
typedef struct LoftSwitch LoftSwitch;
typedef struct LoftWindow LoftWindow;

// enums

enum {
  NORMAL,
  ACTIVE,
  INSENSITIVE,
  N_STATES
};

enum {
  IDLE,
  PRELIGHT,
  SELECTED,
  N_PAIRS
};

enum {
  ASPECT_V,
  ASPECT_H
};

enum {
  ALIGN_LEFT,
  ALIGN_CENTER,
  ALIGN_RIGHT
};

// -- misc.c

struct xy {
  int x,y;
};

struct wh {
  int width,
      height;
};

struct xywh {
  int x,y,
      width,
      height;
};

struct range {
  int start,
      end;
};

void loft_cairo_solid_rectangle (cairo_t *cr, int x, int y, int w, int h);
bool loft_xywh_intersecting (struct xywh *r, struct xywh *a);
bool loft_xy_within (struct xy *p, struct xywh *a);
void loft_xywh_clip (struct xywh *r, struct xywh *a);

// -- data.c

enum {
  TYPE_CHAR,
  TYPE_UCHAR,
  TYPE_WCHAR,
  TYPE_UWCHAR,
  TYPE_INT,
  TYPE_UINT,
  TYPE_LONG,
  TYPE_ULONG,
  TYPE_LONG2,
  TYPE_ULONG2,
  TYPE_FLOAT,
  TYPE_DOUBLE,
  TYPE_PTR,
  TYPE_PTR2
};

struct LoftData {
  int type;
  union {
    char c;
    unsigned char uc;
    int i;
    unsigned int ui;
    long l;
    unsigned long ul;
    long long ll;
    unsigned long long ull;
    float f;
    double d;
    void *ptr;
  };
};

// -- env.c

enum {
  WITHDRAWN,
  RUNNING,
  DEINIT
};

enum {
  LOW,
  MEDIUM,
  HIGH,
  N_UPAIRS
};

struct LoftEnv {
  int state;
  bool no_refresh;

  struct map config;
  struct list objects,
              redraws;

  Display *display;
  int display_fd;
  int screen;
  Window root;

  Atom wm_delete_win;
  Atom wm_state;
  Atom wm_state_fullscreen;
  Atom wm_window_type;
  Atom wm_window_type_dialog;

  int depth;
  Visual *visual;
  Colormap colormap;
  bool rgba;
  unsigned long event_mask;

  struct {
    struct rgba base;
    struct rgba_pair common [N_STATES][N_PAIRS];
    struct rgba_pair urgency [N_STATES][N_UPAIRS];
  } colors;

  struct {
    char *name;
    unsigned int size;
    char *str;
    PangoFontDescription *desc;
  } font;
};

// -- env.c

extern
LoftEnv loft;

void loft_deinit (void);
void loft_init_config (void);
void loft_init (void);
void loft_init_colors (void);
void loft_init_font (char *name, int size);

// -- key.c

struct LoftKey {
  unsigned int state;
  KeySym sym;
  char str[7];
  bool printable;
};

bool loft_str_to_key (char *str, LoftKey *k);

// -- events.c

struct LoftButtonEvent {
  int button;
  unsigned int state;
  union {
    struct { int x,y; };
    struct xy p;
  };
};

struct LoftPointerEvent {
  unsigned int state;
  union {
    struct { int x,y; };
    struct xy p;
  };
};

#define loft_break()  if(loft.state == RUNNING) loft.state = DEINIT
#define loft_lock()   XLockDisplay(loft.display)
#define loft_unlock() XUnlockDisplay(loft.display)

void loft_main (void (*pre)(XEvent *));
bool loft_process (XEvent *e);

// -- refresh.c

void loft_queue_refresh (LoftWidget *w, bool size_update);

// -- signal.c

typedef int (LoftSignalCallback) (void*, void*, void*);

struct LoftSignalHandler {
  bool active;
  char *signal;
  LoftSignalCallback *cb;
  void *data;
  LoftSignalHandler *next;
};

#define loft_connect(OBJ,S,CB,D) _loft_connect(OBJ, S, (LoftSignalCallback*) CB, D)
#define loft_connect_tail(OBJ,S,CB,D) _loft_connect_tail(OBJ, S, (LoftSignalCallback*) CB, D)
#define loft_sh_set_callback(SH,CB) (SH)->cb = (LoftSignalCallback*) CB

LoftSignalHandler *_loft_connect (LoftObject *obj, char *signal, LoftSignalCallback *cb, void *data);
LoftSignalHandler *_loft_connect_tail (LoftObject *obj, char *signal, LoftSignalCallback *cb, void *data);

bool loft_disconnect (LoftObject *obj, LoftSignalHandler *sh);
int loft_emit (LoftObject *obj, char *signal, void *arg);

// -- timer.c

typedef void (LoftTimerCallback) (LoftTimer*, void *data);

struct LoftTimer {
  bool active;
  int timeout;
  LoftTimerCallback *cb;
  void *data;
  pthread_t _thread;
  struct timespec _ts;
};

#define loft_timer_init(T,TI,CB,D) _loft_timer_init(T, TI, (LoftTimerCallback*) CB, D)
#define loft_timer_set_callback(T,CB) (T)->cb = (LoftTimerCallback*) CB
#define loft_timer_set_data(T,D) (T)->data = D

void _loft_timer_init (LoftTimer *t, int timeout, LoftTimerCallback *cb, void *data);

void loft_timer_set_timeout (LoftTimer *t, int timeout);
void loft_timer_start (LoftTimer *t);
void loft_timer_stop (LoftTimer *t);

//
// OBJECTS
//

// -- object.c

struct LoftObject {
  char *type;
  bool freeable;
  LoftSignalHandler *sh;
};

void loft_object_destroy (LoftObject *obj);
void loft_object_init (LoftObject *obj, char *type);
void loft_object_set_freeable (LoftObject *obj, bool freeable);

// -- adj.c

extern
char *ADJUSTMENT;

struct LoftAdjustment {
  LoftObject obj;
  double min,
         max,
         value;
};

void loft_adj_init (LoftAdjustment *adj);
double loft_adj_get_max (LoftAdjustment *adj);
double loft_adj_get_min (LoftAdjustment *adj);
double loft_adj_get_perc (LoftAdjustment *adj);
double loft_adj_get_perc_bounded (LoftAdjustment *adj);
double loft_adj_get_value (LoftAdjustment *adj);
double loft_adj_get_value_bounded (LoftAdjustment *adj);
void loft_adj_set_max (LoftAdjustment *adj, double max);
void loft_adj_set_min (LoftAdjustment *adj, double min);
void loft_adj_set_perc (LoftAdjustment *adj, double perc);
void loft_adj_set_perc_bounded (LoftAdjustment *adj, double perc);
void loft_adj_set_range (LoftAdjustment *adj, double min, double max);
void loft_adj_set_value (LoftAdjustment *adj, double value);
void loft_adj_set_value_bounded (LoftAdjustment *adj, double value);

// -- buffer.c

extern
char *BUFFER;

struct LoftBuffer {
  LoftObject obj;

  char *data;
  int len;
  int alloc_len;

  LoftBufferLine *lines;
  int n_lines;
};

struct LoftBufferLine {
  int num;
  int len;
  int offset;
  char *ptr;
};

void loft_buffer_clear (LoftBuffer *b);
void loft_buffer_delete (LoftBuffer *b, int start, int end);
void loft_buffer_init (LoftBuffer *b, char *data, int len);
int loft_buffer_insert (LoftBuffer *b, int pos, char *data, int len);
LoftBufferLine * loft_buffer_line_from_offset (LoftBuffer *b, int offset);
void loft_buffer_set (LoftBuffer *b, char *data, int len);

// -- builder.c

extern
char *BUILDER;

typedef LoftWidget *(LoftBuilderFunc) (void *data);

struct LoftBuilder {
  LoftObject obj;
  LoftLayout *layout;
  LoftBuilderFunc *func;
};

void loft_builder_configure (LoftBuilder *b, LoftBuilderFunc *bf, LoftLayout *lt);
void loft_builder_clear (LoftBuilder *b);
void loft_builder_init (LoftBuilder *b, LoftBuilderFunc *bf, LoftLayout *lt);
LoftBuilder * loft_builder_new (LoftBuilderFunc *bf, LoftLayout *lt);
void loft_builder_apply (LoftBuilder *b, void *data, size_t size, size_t len);

// -- text_tag.c

enum {
  ATTR_FONT_DESCRIPTION =     1 << 0,
  ATTR_FAMILY =               1 << 1,
  ATTR_BACKGROUND =           1 << 2,
  ATTR_FOREGROUND =           1 << 3,
  ATTR_SIZE =                 1 << 4,
  ATTR_WEIGHT =               1 << 5,
  ATTR_STYLE =                1 << 6,
  ATTR_UNDERLINE =            1 << 7,
  ATTR_UNDERLINE_COLOR =      1 << 8,
  ATTR_STRIKETHROUGH =        1 << 9,
  ATTR_STRIKETHROUGH_COLOR =  1 << 10,
  ATTR_VARIANT =              1 << 11,
  ATTR_LETTER_SPACING =       1 << 12,
  ATTR_STRETCH =              1 << 13,
  ATTR_SCALE =                1 << 14,
  ATTR_RISE =                 1 << 15,
  ATTR_GRAVITY =              1 << 16,
  ATTR_GRAVITY_HINT =         1 << 17
};

struct LoftTextTag {
  unsigned int attrs;

  char *font_desc;
  char *family;

  struct rgba background;
  struct rgba foreground;

  double size;
  int weight;
  int style;

  int underline;
  struct rgba underline_color;

  int strikethrough;
  struct rgba strikethrough_color;

  int variant;
  int letter_spacing;
  int stretch;
  double scale;
  double rise;

  int gravity;
  int gravity_hint;
};

void loft_text_tag_init (LoftTextTag *t, unsigned int attrs);

// -- text_tag_tb.c

extern
char *TEXT_TAG_TABLE;

struct LoftTextTagData {
  LoftTextTag *tag;
  struct range *ranges;
  int n_ranges;
  int n_alloc_ranges;
};

struct LoftTextTagTable {
  LoftObject obj;
  LoftTextTagData *data;
  int len;
  int alloc_len;
};

bool loft_text_tag_table_add_tag (LoftTextTagTable *tt, LoftTextTag *t);
bool loft_text_tag_table_clear_range (LoftTextTagTable *tt, LoftTextTag *t, int start, int end);
bool loft_text_tag_table_clear_tag (LoftTextTagTable *tt, LoftTextTag *t);
void loft_text_tag_table_init (LoftTextTagTable *tt);
bool loft_text_tag_table_rm_tag (LoftTextTagTable *tt, LoftTextTag *t);
bool loft_text_tag_table_set_range (LoftTextTagTable *tt, LoftTextTag *t, int start, int end);
PangoAttrList * loft_text_tag_table_to_attr_list (LoftTextTagTable *tt);

//
// WIDGETS
//

// -- widget.c

enum {
  SIZE_H4W,
  SIZE_STATIC
};

enum {
  EXPAND_X =          1 << 13,
  EXPAND_Y =          1 << 14,

  PACK_X =            1 << 15,
  PACK_Y =            1 << 16,

  FLOW_L =            1 << 17,
  FLOW_U =            1 << 18,
  FLOW_R =            1 << 19,
  FLOW_D =            1 << 20,

  EXPAND =            EXPAND_X | EXPAND_Y,
  PACK =              PACK_X   | PACK_Y
};

struct LoftWidget {
  LoftObject obj;

  unsigned int attrs;
  int size_mode;

  union {
    struct {
      uint8_t dirty,
              stale;
      int x,      y,
          width,  height,
          old_x,  old_y,
          old_w,  old_h,
          buf_w,  buf_h,
          min_w,  min_h,
          pref_w, pref_h;
    };
    struct {
      uint16_t redraw;
      union {
        struct {
          struct xy pos;
          struct wh size;
          struct xy old_pos;
          struct wh old_size;
        };
        struct {
          struct xywh geo,
                      old_geo;
        };
      };

      struct wh buf_size,
                min_size,
                pref_size;
    };
  };

  bool drawable,
       dragging,
       prelight,
       pressed,
       sensitive,
       visible,
       can_clip,
       dirty_clip,
       can_focus,
       has_focus,
       has_input_focus;

  LoftWidget *child;
  LoftWidget *next;
  LoftWidget *parent;
  int n_children;

  struct xywh clip,
              clip_adj;

  cairo_surface_t *buffer;
  struct rgba_pair colors [N_STATES][N_PAIRS];
};

void loft_widget_attach (LoftWidget *w, LoftWidget *p, int pos);
void loft_widget_base_size (LoftWidget *w, int *base_w, int *base_h);
bool loft_widget_can_clip (LoftWidget *w);
bool loft_widget_can_focus (LoftWidget *w);
void loft_widget_configure (LoftWidget *w, int x, int y, int width, int height);
void loft_widget_detach (LoftWidget *w);
void loft_widget_detach_children (LoftWidget *w);
void loft_widget_focus (LoftWidget *w);
unsigned int loft_widget_get_attrs (LoftWidget *w);
LoftWidget * loft_widget_get_child_at_xy (LoftWidget *w, struct xy *p);
int loft_widget_grow_buffer (LoftWidget *w, int width, int height);
bool loft_widget_has_focus (LoftWidget *w);
bool loft_widget_has_input_focus (LoftWidget *w);
void loft_widget_hide (LoftWidget *w);
void loft_widget_hide_all (LoftWidget *w);
void loft_widget_init (LoftWidget *w, char *type, bool visible, bool drawable, int size_mode);
bool loft_widget_is_dirty (LoftWidget *w);
bool loft_widget_is_drawable (LoftWidget *w);
bool loft_widget_is_prelight (LoftWidget *w);
bool loft_widget_is_pressed (LoftWidget *w);
bool loft_widget_is_sensitive (LoftWidget *w);
bool loft_widget_is_stale (LoftWidget *w);
bool loft_widget_is_set_visible (LoftWidget *w);
bool loft_widget_is_visible (LoftWidget *w);
bool loft_widget_native (LoftWidget *w, LoftWidget* *n);
void loft_widget_refresh (LoftWidget *w, bool size_update);
void loft_widget_refresh_fast (LoftWidget *w);
void loft_widget_set_attrs (LoftWidget *w, unsigned int attrs);
void loft_widget_set_drawable (LoftWidget *w, bool drawable);
void loft_widget_set_min_size (LoftWidget *w, int min_w, int min_h);
void loft_widget_set_pref_size (LoftWidget *w, int pref_w, int pref_h);
void loft_widget_set_sensitive (LoftWidget *w, bool sensitive);
void loft_widget_show (LoftWidget *w);
void loft_widget_show_all (LoftWidget *w);
bool loft_widget_touching (LoftWidget *w, struct xy *p);
void loft_widget_unfocus (LoftWidget *w);

// -- button.c

extern
char *BUTTON;

struct LoftButton {
  LoftWidget w;

  int alignment,
      padding;
  bool active,
       activatable,
       pulse,
       use_markup;
  char *str;

  double pulse_fade [2];
  unsigned int pulse_timeout;

  PangoLayout *_lt;
  bool _draw_pulse;
  LoftTimer _pulse_timer;
};

int loft_button_get_alignment (LoftButton *btn);
int loft_button_get_padding (LoftButton *btn);
double * loft_button_get_pulse_fade (LoftButton *btn);
int loft_button_get_pulse_timeout (LoftButton *btn);
void loft_button_init (LoftButton *btn, char *str, bool use_markup);
bool loft_button_is_activatable (LoftButton *btn);
bool loft_button_is_active (LoftButton *btn);
void loft_button_set_active (LoftButton *btn, bool active);
void loft_button_set_activatable (LoftButton *btn, bool activatable);
void loft_button_set_alignment (LoftButton *btn, int alignment);
void loft_button_set_padding (LoftButton *btn, int padding);
void loft_button_set_pulse (LoftButton *btn, bool pulse);
void loft_button_set_pulse_fade (LoftButton *btn, double value, double max);
void loft_button_set_pulse_timeout (LoftButton *btn, int pulse_timeout);
void loft_button_set_str (LoftButton *btn, char *str, bool use_markup);

// -- colourbox.c

extern
char *COLOURBOX;

struct LoftColourBox {
  LoftWidget w;
  struct rgba colour;
};

void loft_colourbox_init    (LoftColourBox *c);
void loft_colourbox_set     (LoftColourBox *c, struct rgba *v);
void loft_colourbox_set_str (LoftColourBox *c, char *str);

// -- editable.c

extern
char *EDITABLE;

struct LoftEditable {
  LoftWidget w;

  LoftBuffer *buffer;
  LoftTextTagTable *tag_table;

  int position;
  int line_offset;
  int max_position;

  bool has_selection;
  struct range selection;

  bool line_wrap;
  int padding;
  char *pretext;

  int cursor_blink_timeout;
  struct rgba cursor_color;

  struct xywh _cursor_pos;
  bool _draw_cursor;

  LoftTimer _cursor_blinker;

  LoftSignalHandler *_buffer_on_changed;
  LoftSignalHandler *_tt_on_changed;

  PangoLayout *_lt;
  PangoAttrList *_attrs_unused;
  int _init_sel_offset;
  LoftTextTag _selection_tag;
};

void loft_editable_clear_selection (LoftEditable *e);
LoftBuffer * loft_editable_get_buffer (LoftEditable *e);
bool loft_editable_get_line_wrap (LoftEditable *e);
int loft_editable_get_padding (LoftEditable *e);
char * loft_editable_get_pretext (LoftEditable *e);
struct range loft_editable_get_selection (LoftEditable *e);
LoftTextTagTable * loft_editable_get_tag_table (LoftEditable *e);
bool loft_editable_has_selection (LoftEditable *e);
void loft_editable_init (LoftEditable *e, LoftBuffer *buffer, char *pretext);
void loft_editable_set_buffer (LoftEditable *e, LoftBuffer *buffer);
void loft_editable_set_cursor_blink_timeout (LoftEditable *e, int cursor_blink_timeout);
void loft_editable_set_cursor_position (LoftEditable *txtv, int position);
void loft_editable_set_line_wrap (LoftEditable *e, bool line_wrap);
void loft_editable_set_padding (LoftEditable *e, int padding);
void loft_editable_set_pretext (LoftEditable *e, char *pretext);
void loft_editable_set_selection (LoftEditable *e, int start, int end);
void loft_editable_set_tag_table (LoftEditable *e, LoftTextTagTable *tt);
void loft_editable_set_use_markup (LoftEditable *e, bool use_markup);

// -- label.c

extern
char *LABEL;

enum {
  ELLIPSIZE_NONE,
  ELLIPSIZE_START,
  ELLIPSIZE_MIDDLE,
  ELLIPSIZE_END
};

struct LoftLabel {
  LoftWidget w;

  int alignment;
  int bg_slant;
  bool draw_bg;
  int ellipsize;
  bool highlighted;
  bool pulse;
  char *str;
  bool use_markup;
  bool word_wrap;

  struct {
    int left,
        top,
        right,
        bottom;
  } padding;

  double pulse_fade [2];
  unsigned int pulse_timeout;

  PangoLayout *_lt;

  bool _draw_pulse;
  LoftTimer _pulse_timer;
};

int loft_label_get_bg_slant (LoftLabel *lb);
int loft_label_get_ellipsize (LoftLabel *lb);
bool loft_label_get_word_wrap (LoftLabel *lb);
char * loft_label_get_str (LoftLabel *lb);
bool loft_label_get_use_markup (LoftLabel *lb);
void loft_label_init (LoftLabel *lb, char *str, bool use_markup);
bool loft_label_is_highlighted (LoftLabel *lb);
LoftLabel * loft_label_new (char *str, bool use_markup);
void loft_label_set_alignment (LoftLabel *lb, int alignment);
void loft_label_set_bg_slant (LoftLabel *lb, int bg_slant);
void loft_label_set_draw_bg (LoftLabel *lb, bool draw_bg);
void loft_label_set_ellipsize (LoftLabel *lb, int ellipsize);
void loft_label_set_highlighted (LoftLabel *lb, bool highlighted);
void loft_label_set_padding (LoftLabel *lb, int l, int t, int r, int b);
void loft_label_set_pulse (LoftLabel *lb, bool pulse);
void loft_label_set_pulse_fade (LoftLabel *lb, double value, double max);
void loft_label_set_pulse_timeout (LoftLabel *lb, int pulse_timeout);
void loft_label_set_str (LoftLabel *lb, char *str, bool use_markup);
void loft_label_set_word_wrap (LoftLabel *lb, bool word_wrap);

// -- layout.c

extern
char *LAYOUT;

struct LoftLayout {
  LoftWidget w;

  int aspect;
  int spacing;

  LoftSignalHandler *_arrange;
  LoftSignalHandler *_size_request;
};

void loft_layout_init (LoftLayout *lt, int aspect, int spacing);
void loft_layout_init_with_type (LoftLayout *lt, char *type, bool drawable, int aspect, int spacing);
LoftLayout * loft_layout_new (int aspect, int spacing);
LoftLayout * loft_layout_new_with_type (char *type, bool drawable, int aspect, int spacing);
void loft_layout_set_aspect (LoftLayout *lt, int aspect);
void loft_layout_set_spacing (LoftLayout *lt, int spacing);

// -- scale.c

extern
char *SCALE;

struct LoftScale {
  LoftWidget w;

  int aspect;
  double fill_div;
  bool inverted;
  double step;

  LoftAdjustment *adj;

  struct {
    int min,
        max;
  } span;

  int pos;
  struct xywh sl_geo;
  bool sl_prelight;
  int drag_offset;
  struct xy pointer;

  LoftSignalHandler *_adj_on_changed;
};

int loft_scale_get_aspect (LoftScale *sc);
bool loft_scale_has_adj (LoftScale *sc);
void loft_scale_init (LoftScale *sc, int aspect, LoftAdjustment *adj);
bool loft_scale_is_inverted (LoftScale *sc);
void loft_scale_set_adj (LoftScale *sc, LoftAdjustment *adj);
void loft_scale_set_aspect (LoftScale *sc, int aspect);
void loft_scale_set_inverted (LoftScale *sc, bool inverted);
void loft_scale_set_step (LoftScale *sc, double step);

// -- scrollv.c

extern
char *SCROLLV;

extern
int SCROLL_X;
extern
int SCROLL_Y;
extern
int SCROLL_BI;

struct LoftScrollView {
  LoftWidget w;

  int border_size;
  int policy;
  int slider_size;
  int step;

  LoftAdjustment x_adj,
                 y_adj;

  struct wh span;

  struct xywh x_sl_geo,
              y_sl_geo;

  bool x_sl_prelight,
       y_sl_prelight;

  int drag_offset;
  int drag_orientation;

  struct xy pointer;

  LoftSignalHandler *_child_resize;
  LoftSignalHandler *_child_set_focus;
  LoftSignalHandler *_child_set_poi;
};

int loft_scrollv_get_step (LoftScrollView *sv);
int loft_scrollv_get_x_offset (LoftScrollView *sv);
int loft_scrollv_get_y_offset (LoftScrollView *sv);
void loft_scrollv_init (LoftScrollView *sv, int policy);
void loft_scrollv_scroll_x (LoftScrollView *sv, int amount);
void loft_scrollv_scroll_y (LoftScrollView *sv, int amount);
void loft_scrollv_set_border_size (LoftScrollView *sv, int border_size);
void loft_scrollv_set_policy (LoftScrollView *sv, int policy);
void loft_scrollv_set_slider_size (LoftScrollView *sv, int slider_size);
void loft_scrollv_set_step (LoftScrollView *sv, int step);
void loft_scrollv_set_x_offset (LoftScrollView *sv, int x_offset);
void loft_scrollv_set_y_offset (LoftScrollView *sv, int y_offset);

// -- selectable.c

struct LoftSelectable {
  LoftLayout lt;
  bool selected;
  void *data;
};

void loft_selectable_init (LoftSelectable *sel, int aspect, int spacing, void *data);
LoftSelectable * loft_selectable_new (int aspect, int spacing, void *data);
void loft_selectable_set (LoftSelectable *sel, bool selected);
void loft_selectable_set_exclusive (LoftSelectable *sel, bool selected);
void loft_selection_clear (LoftSelectable *sel);

// -- switch.c

extern
char *SWITCH;

enum {
  SWITCH_CHECK,
  SWITCH_RADIO,
  SWITCH_FANCY
};

struct LoftSwitch {
  LoftWidget w;

  int type;
  bool active;

  LoftSwitch *head;
  LoftSwitch *next;
};

void loft_switch_init (LoftSwitch *sw, int type);
void loft_switch_set_active (LoftSwitch *sw, bool active);
void loft_switch_set_type (LoftSwitch *sw, int type);

// -- window.c

extern
char *WINDOW;

enum {
  CONSTRAIN_W = 1,
  CONSTRAIN_H = 2
};

struct LoftWindow {
  LoftWidget w;

  Window xwin;
  cairo_surface_t *target;
  LoftWidget *focused;
  unsigned int constraints;
  int padding;
  bool size_propagated;
  struct rgba base_color;

  int _n_stale;
  int _n_alloc_stale;
  struct xywh *_stale;
};

#define loft_window_configure(win,x,y,w,h) XMoveResizeWindow(loft.display, (win)->xwin, x, y, w, h)
#define loft_window_hide(win) XUnmapWindow(loft.display, (win)->xwin)
#define loft_window_lower(win) XLowerWindow(loft.display, (win)->xwin)
#define loft_window_move(win,x,y) XMoveWindow(loft.display, (win)->xwin, x, y)
#define loft_window_raise(win) XRaiseWindow(loft.display, (win)->xwin)
#define loft_window_resize(win,w,h) XResizeWindow(loft.display, (win)->xwin, w, h)
#define loft_window_set_name(win,name) XStoreName(loft.display, win->xwin, name);
#define loft_window_show(win) XMapWindow(loft.display, (win)->xwin)

LoftWindow * loft_window_from_xwin (Window xwin);
bool loft_window_get_override_redirect (LoftWindow *win);
void loft_window_hide_all (LoftWindow *win);
void loft_window_init (LoftWindow *win, char *name, int padding, bool override_redirect);
void loft_window_set_constraints (LoftWindow *win, unsigned int constraints);
void loft_window_set_dialog (LoftWindow *win, bool dialog);
void loft_window_set_fullscreen (LoftWindow *win, bool fullscreen);
void loft_window_set_padding (LoftWindow *win, int padding);
void loft_window_set_override_redirect (LoftWindow *win, bool override_redirect);
void loft_window_set_urgent (LoftWindow *win, bool urgent);
void loft_window_show_all (LoftWindow *win);

#endif
