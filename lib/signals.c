#include "loft.h"

LoftSignalHandler * _loft_connect (LoftObject *obj, char *signal, LoftSignalCallback *cb, void *data) {
  LoftSignalHandler *sh = malloc(sizeof(LoftSignalHandler));
  if(sh == NULL)
    return NULL;

  sh->active = true;
  sh->signal = signal;
  sh->cb = cb;
  sh->data = data;
  sh->next = obj->sh;

  obj->sh = sh;

  return sh;
}

LoftSignalHandler * _loft_connect_tail (LoftObject *obj, char *signal, LoftSignalCallback *cb, void *data) {
  LoftSignalHandler *sh = malloc(sizeof(LoftSignalHandler));
  if(sh == NULL)
    return NULL;

  sh->active = true;
  sh->signal = signal;
  sh->cb = cb;
  sh->data = data;
  sh->next = NULL;

  if(obj->sh != NULL) {
    LoftSignalHandler *t;
    for(t = obj->sh; t != NULL; t = t->next) {
      if(t->next == NULL)
        break;
    }
    t->next = sh;
  }
  else {
    obj->sh = sh;
  }

  return sh;
}

bool loft_disconnect (LoftObject *obj, LoftSignalHandler *sh) {
  if(obj->sh == sh) {
    obj->sh = sh->next;
    free(sh);
    return true;
  }

  LoftSignalHandler *h;
  LoftSignalHandler *hp;

  for(h = obj->sh; h != NULL; h = h->next) {
    if(h->next == sh) {
      hp = h;
      h = h->next;
      hp->next = h->next;
      free(h);
      return true;
    }
  }

  return false;
}

int loft_emit (LoftObject *obj, char *signal, void *arg) {
  int ret = 0;

  LoftSignalHandler *sh;
  for(sh = obj->sh; sh != NULL; sh = sh->next) {
    if(sh->active == false || sh->cb == NULL || strcmp(sh->signal, signal) != 0)
      continue;

    ret = sh->cb(obj, arg, sh->data);

    //  If the object was destroyed from within a callback, the
    //  linked signal handler list will be NULL and this loop
    //  will terminate to avoid a segfault.

    if(ret == 1 || obj->sh == NULL)
      break;
  }

  return ret;
}
