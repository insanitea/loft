#include "loft.h"
#include <time.h>

char *TIMER = "loft-timer";

void * _timer_loop (void *arg) {
  LoftTimer *t = arg;

  while(t->active) {
    nanosleep(&t->_ts, NULL);
    pthread_setcancelstate(PTHREAD_CANCEL_DISABLE, NULL);
    if(t->cb != NULL) {
      loft_lock();
      t->cb(t, t->data);
      loft_unlock();
    }
    pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, NULL);
  }

  return NULL;
}

void _loft_timer_init (LoftTimer *t, int timeout, LoftTimerCallback *cb, void *data) {
  loft_timer_set_timeout(t, timeout);
  t->active = false;
  t->cb = cb;
  t->data = data;
}

int _timer_on_destroy (LoftTimer *t, void *arg, void *data) {
  return 1;
}

// --

void loft_timer_set_timeout (LoftTimer *t, int timeout) {
  t->timeout = timeout;

  if(t->timeout >= 1000)
    t->_ts.tv_sec = t->timeout / 1000;
  else
    t->_ts.tv_sec = 0;

  t->_ts.tv_nsec = (t->timeout * 1000000L) - (t->_ts.tv_sec * 1000000000L);
}

void loft_timer_start (LoftTimer *t) {
  if(t->active)
    return;

  t->active = true;
  pthread_create(&t->_thread, NULL, _timer_loop, t);
}

void loft_timer_stop (LoftTimer *t) {
  if(t->active == false)
    return;

  pthread_cancel(t->_thread);
  t->active = false;
}
