#include "loft.h"

void _object_destroy (LoftObject *obj) {
  debug(B_RED "DESTROY " C_ESC B_WHITE "%s" C_ESC " @ " B_WHITE "%p\n" C_ESC, obj->type, obj);

  if(loft_emit(obj, "destroy", NULL) == 1 && loft.state == RUNNING)
    return;

  LoftSignalHandler *sh;
  while(obj->sh != NULL) {
    sh = obj->sh;
    obj->sh = sh->next;
    free(sh);
  }

  if(obj->freeable)
    free(obj);
}

void loft_object_destroy (LoftObject *obj) {
  _object_destroy(obj);
  list_remove(&loft.objects, obj);
}

void loft_object_init (LoftObject *obj, char *type) {
  obj->type = type;
  obj->sh = NULL;
  obj->freeable = false;
  list_append(&loft.objects, obj, NULL);
}

void loft_object_set_freeable (LoftObject *obj, bool freeable) {
  obj->freeable = freeable;
}
