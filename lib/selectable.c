#include "loft.h"
#include <stdarg.h>

char *SELECTABLE = "loft-selectable";

int _selectable_on_draw (LoftWidget *w, cairo_t *cr, void *data) {
  LoftSelectable *sel = (LoftSelectable*) w;

  int state;
  if(w->sensitive)
    state = NORMAL;
  else
    state = INSENSITIVE;

  struct rgba *bg;
  if(sel->selected)
    bg = &w->colors[state][SELECTED].bg;
  else
    bg = &w->colors[state][IDLE].bg;

  rgba_cairo_set(cr, bg);
  cairo_rectangle(cr, 0, 0, w->width, w->height);
  cairo_fill(cr);

  return 0;
}

void _selectable_set (LoftSelectable *sel, bool selected) {
  sel->selected = selected;
  sel->lt.w.redraw = true;
  loft_emit(&sel->lt.w.obj, "set-selected", &sel->selected);
}

void _selection_clear (LoftWidget *w) {
  LoftSelectable *sel;
  while(w != NULL) {
    if(w->obj.type == SELECTABLE) {
      sel = (LoftSelectable*) w;
      if(sel->selected)
        _selectable_set(sel, false);
    }
    w = w->next;
  }
}

// --

void loft_selectable_init (LoftSelectable *sel, int aspect, int spacing, void *data) {
  loft_layout_init_with_type(&sel->lt, SELECTABLE, true, aspect, spacing);
  loft_connect(&sel->lt.w.obj, "draw", _selectable_on_draw, NULL);
  sel->selected = false;
  sel->data = data;
}

LoftSelectable *loft_selectable_new (int aspect, int spacing, void *data) {
  LoftSelectable *sel = malloc(sizeof(LoftSelectable));
  if(sel == NULL)
    return NULL;

  loft_selectable_init(sel, aspect, spacing, data);
  loft_object_set_freeable(&sel->lt.w.obj, true);

  return sel;
}

void loft_selectable_set (LoftSelectable *sel, bool selected) {
  if(sel->selected == selected)
    return;

  _selectable_set(sel, selected);
  loft_widget_refresh(&sel->lt.w, true);
}

void loft_selectable_set_exclusive (LoftSelectable *sel, bool selected) {
  _selection_clear(&sel->lt.w);
  _selectable_set(sel, selected);
  loft_widget_refresh(&sel->lt.w, true);
}

void loft_selectable_unset (LoftSelectable *sel) {
  if(sel->selected == false)
    return;

  sel->selected = false;
  loft_widget_refresh(&sel->lt.w, true);
}

// selection management

void loft_selection_clear (LoftSelectable *sel) {
  LoftWidget *p = sel->lt.w.parent;
  LoftWidget *fs;

  if(p != NULL)
    fs = p->child;
  else
    fs = &sel->lt.w;

  _selection_clear(fs);

  if(p != NULL)
    loft_widget_refresh(p, true);
}

LoftSelectable *loft_selection_next (LoftSelectable *sel) {
  while(sel != NULL && sel->selected == false)
    sel = (LoftSelectable*) sel->lt.w.next;

  return sel;
}
