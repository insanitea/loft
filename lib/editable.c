#include "loft.h"
#include <clip/utf8.h>
#include <ctype.h>

char *EDITABLE = "loft-editable";

void _editable_bump_cursor (LoftEditable *e) {
  if(e->w.has_input_focus == false)
    return;

  loft_timer_stop(&e->_cursor_blinker);
  e->_draw_cursor = true;
  loft_timer_start(&e->_cursor_blinker);
}

void _editable_update_attrs (LoftEditable *e) {
  if(e->buffer != NULL && e->buffer->len > 0) {
    PangoAttrList *attrs = loft_text_tag_table_to_attr_list(e->tag_table);

    if(e->has_selection) {
      LoftTextTag *st = &e->_selection_tag;
      PangoAttribute *sel_attr;
      struct rgba_values cv;

      rgba_values(&st->background, &cv, RGBA_16);
      sel_attr = pango_attr_background_new(cv.r, cv.g, cv.b);
      sel_attr->start_index = e->selection.start;
      sel_attr->end_index = e->selection.end + 1;
      pango_attr_list_insert(attrs, sel_attr);

      rgba_values(&st->foreground, &cv, RGBA_16);
      sel_attr = pango_attr_foreground_new(cv.r, cv.g, cv.b);
      sel_attr->start_index = e->selection.start;
      sel_attr->end_index = e->selection.end + 1;
      pango_attr_list_insert(attrs, sel_attr);
    }

    pango_layout_set_attributes(e->_lt, attrs);
    pango_attr_list_unref(attrs);
  }
  else {
    if(e->_attrs_unused != NULL)
      pango_attr_list_unref(e->_attrs_unused);

    e->_attrs_unused = loft_text_tag_table_to_attr_list(e->tag_table);
  }
}

void _editable_update_cursor (LoftEditable *e) {
  int byte_pos;
  if(e->buffer != NULL && e->buffer->data != NULL) {
    if(e->position <= e->buffer->len)
      byte_pos = utf8_index_to_byte(e->buffer->data, e->position);
    else
      byte_pos = e->buffer->len;
  }
  else {
    byte_pos = 0;
  }

  PangoRectangle c;
  pango_layout_get_cursor_pos(e->_lt, byte_pos, &c, NULL);

  e->_cursor_pos.x = e->padding + (c.x / PANGO_SCALE);
  e->_cursor_pos.y = e->padding + (c.y / PANGO_SCALE);
  e->_cursor_pos.width = 1;
  e->_cursor_pos.height = c.height / PANGO_SCALE;
}

int _editable_update_layout (LoftEditable *e) {
  if(e->buffer != NULL && e->buffer->len > 0) {
    if(e->_attrs_unused != NULL) {
      pango_layout_set_attributes(e->_lt, e->_attrs_unused);
      pango_attr_list_unref(e->_attrs_unused);
      e->_attrs_unused = NULL;
    }

    pango_layout_set_text(e->_lt, e->buffer->data, e->buffer->len);
    return 2;
  }
  else {
    if(e->_attrs_unused == NULL) {
      e->_attrs_unused = pango_layout_get_attributes(e->_lt);
      if(e->_attrs_unused != NULL) {
        pango_attr_list_ref(e->_attrs_unused);
        pango_layout_set_attributes(e->_lt, NULL);
      }
    }

    if(e->pretext != NULL) {
      pango_layout_set_text(e->_lt, e->pretext, -1);
      return 1;
    }
    else {
      pango_layout_set_text(e->_lt, NULL, 0);
      return 0;
    }
  }
}

int _editable_xy_to_offset (LoftEditable *e, int x, int y, bool *exact) {
  int lt_x = (x - e->w.x - e->padding) * PANGO_SCALE;
  int lt_y = (y - e->w.y - e->padding) * PANGO_SCALE;

  if(lt_x < 0)
    lt_x = 0;

  if(lt_y < 0)
    lt_y = 0;

  int byte, trailing;
  bool _exact = pango_layout_xy_to_index(e->_lt, lt_x, lt_y, &byte, &trailing);

  if(exact != NULL)
    *exact = _exact;

  return utf8_byte_to_index(e->buffer->data, byte + trailing);
}

void _editable_clear_selection (LoftEditable *e) {
  if(e->has_selection) {
    e->has_selection = false;
    e->selection = (struct range) { -1,-1 };
    _editable_update_attrs(e);
  }
}

// --

int _editable_on_button_press (LoftEditable *e, LoftButtonEvent *be, void *data) {
  if(e->buffer == NULL || e->buffer->len == 0 || be->button != 1)
    return 0;

  bool exact;
  int init_byte = _editable_xy_to_offset(e, be->x, be->y, &exact);

  _editable_clear_selection(e);

  e->_init_sel_offset = utf8_byte_to_index(e->buffer->data, init_byte);
  e->position = e->_init_sel_offset;

  if(exact) {
    LoftBufferLine *cl = loft_buffer_line_from_offset(e->buffer, e->position);
    e->line_offset = e->position - cl->offset;
  }
  else {
    e->line_offset = -1;
  }

  _editable_update_cursor(e);
  loft_emit(&e->w.obj, "set-poi", &e->_cursor_pos);

  if(e->w.has_input_focus == false) {
    loft_widget_focus(&e->w);
  }
  else {
    _editable_bump_cursor(e);
    loft_widget_refresh(&e->w, true);
  }

  return 2;
}

int _editable_on_deinit (LoftEditable *e, void *arg, void *data) {
  loft_timer_stop(&e->_cursor_blinker);
  g_object_unref(e->_lt);
  return 0;
}

int _editable_on_drag_motion (LoftEditable *e, LoftPointerEvent *pe, void *data) {
  int offset = _editable_xy_to_offset(e, pe->x, pe->y, NULL);

  if(offset != e->position) {
    e->position = offset;

    if(offset == e->_init_sel_offset) {
      e->has_selection = false;
      e->selection = (struct range) { -1,-1 };
    }
    else if(offset < e->_init_sel_offset) {
      e->selection = (struct range) { offset, e->_init_sel_offset - 1 };
      e->has_selection = true;
    }
    else if(offset > e->_init_sel_offset) {
      e->selection = (struct range) { e->_init_sel_offset, offset - 1 };
      e->has_selection = true;
    }

    _editable_update_attrs(e);
    _editable_update_cursor(e);

    bool poi_set = loft_emit(&e->w.obj, "set-poi", &e->_cursor_pos);
    _editable_bump_cursor(e);

    if(poi_set == false)
      loft_widget_refresh_fast(&e->w);
    else
      loft_widget_refresh(&e->w, false);
  }

  return 0;
}

int _editable_on_draw (LoftEditable *e, cairo_t *cr, void *data) {
  int state;
  if(e->w.sensitive && e->buffer != NULL && e->buffer->len > 0)
    state = NORMAL;
  else
    state = INSENSITIVE;

  rgba_cairo_set(cr, &e->w.colors[state][IDLE].fg);
  cairo_move_to(cr, e->padding, e->padding);
  pango_cairo_update_layout(cr, e->_lt);
  pango_cairo_show_layout(cr, e->_lt);

  if(e->_draw_cursor) {
    rgba_cairo_set(cr, &e->w.colors[ACTIVE][IDLE].fg);
    cairo_rectangle (
        cr,
        e->_cursor_pos.x,
        e->_cursor_pos.y,
        e->_cursor_pos.width,
        e->_cursor_pos.height
        );
    cairo_fill(cr);
  }

  return 0;
}

int _editable_on_key_press (LoftEditable *e, LoftKey *k, void *data) {
  if(e->buffer == NULL || e->w.has_focus == false)
    return 0;

  if(k->sym == XK_BackSpace) {
    int new_pos, start, end;

    if(e->has_selection) {
      new_pos = e->selection.start;
      start = utf8_index_to_byte(e->buffer->data, e->selection.start);
      end = utf8_index_to_byte(e->buffer->data, e->selection.end);
      e->max_position -= (e->selection.end - e->selection.start) + 1;

      e->selection = (struct range) { -1,-1 };
      _editable_update_attrs(e);
    }
    else {
      if(e->position == 0)
        return 0;

      new_pos = e->position - 1;
      start = utf8_index_to_byte(e->buffer->data, new_pos);
      end = start + utf8_byte_count(e->buffer->data[start]) - 1;
      e->max_position -= 1;
    }

    if(loft_emit(&e->w.obj, "backspace", &new_pos) == 1)
      return 0;
    if(loft_emit(&e->w.obj, "move-cursor", &new_pos) == 1)
      return 0;

    e->_buffer_on_changed->active = false;
    loft_buffer_delete(e->buffer, start, end);
    _editable_update_layout(e);
    e->_buffer_on_changed->active = true;

    e->position = new_pos;
    _editable_update_cursor(e);
    loft_emit(&e->w.obj, "set-poi", &e->_cursor_pos);

    _editable_bump_cursor(e);
    loft_widget_refresh(&e->w, true);

    return 1;
  }
  else if(k->sym == XK_Up) {
    _editable_clear_selection(e);

    LoftBufferLine *cl = loft_buffer_line_from_offset(e->buffer, e->position);
    if(cl == NULL || cl->num == 0)
      return 0;

    LoftBufferLine *prev_line = &e->buffer->lines[cl->num - 1];
    if(prev_line == NULL)
      return 0;

    int new_pos;
    if(e->line_offset >= 0 && e->line_offset <= prev_line->len)
      new_pos = prev_line->offset + e->line_offset;
    else
      new_pos = prev_line->offset + prev_line->len;

    if(loft_emit(&e->w.obj, "move-cursor", &new_pos) == 1)
      return 0;

    e->position = new_pos;

    _editable_update_cursor(e);
    loft_emit(&e->w.obj, "set-poi", &e->_cursor_pos);

    _editable_bump_cursor(e);
    loft_widget_refresh(&e->w, false);

    return 1;
  }
  else if(k->sym == XK_Down) {
    _editable_clear_selection(e);

    LoftBufferLine *cl = loft_buffer_line_from_offset(e->buffer, e->position);
    if(cl == NULL || cl->num == e->buffer->n_lines - 1)
      return 0;

    LoftBufferLine *nl = &e->buffer->lines[cl->num + 1];
    if(nl == NULL)
      return 0;

    int new_pos;
    if(e->line_offset >= 0 && e->line_offset <= nl->len)
      new_pos = nl->offset + e->line_offset;
    else
      new_pos = nl->offset + nl->len;

    if(loft_emit(&e->w.obj, "move-cursor", &new_pos) == 1)
      return 0;

    e->position = new_pos;

    _editable_update_cursor(e);
    loft_emit(&e->w.obj, "set-poi", &e->_cursor_pos);

    _editable_bump_cursor(e);
    loft_widget_refresh(&e->w, false);

    return 1;
  }
  else if(k->sym == XK_Home) {
    _editable_clear_selection(e);

    LoftBufferLine *cl = loft_buffer_line_from_offset(e->buffer, e->position);
    if(cl == NULL)
      return 0;

    e->line_offset = 0;
    int new_pos = cl->offset;

    if(loft_emit(&e->w.obj, "move-cursor", &new_pos) == 1)
      return 0;

    if(e->position == new_pos)
      return 0;

    e->position = new_pos;

    _editable_update_cursor(e);
    loft_emit(&e->w.obj, "set-poi", &e->_cursor_pos);

    _editable_bump_cursor(e);
    loft_widget_refresh(&e->w, false);

    return 1;
  }
  else if(k->sym == XK_End) {
    _editable_clear_selection(e);

    LoftBufferLine *cl = loft_buffer_line_from_offset(e->buffer, e->position);
    if(cl == NULL)
      return 0;

    e->line_offset = -1;
    int new_pos = cl->offset + cl->len;

    if(e->position == new_pos)
      return 0;

    if(loft_emit(&e->w.obj, "move-cursor", &new_pos) == 1)
      return 0;

    e->position = new_pos;

    _editable_update_cursor(e);
    loft_emit(&e->w.obj, "set-poi", &e->_cursor_pos);

    _editable_bump_cursor(e);
    loft_widget_refresh(&e->w, false);

    return 1;
  }
  else if(k->sym == XK_Left) {
    _editable_clear_selection(e);

    if(e->position == 0)
      return 0;

    LoftBufferLine *cl = loft_buffer_line_from_offset(e->buffer, e->position);
    if(cl == NULL)
      return 0;

    int new_pos = e->position - 1;
    if(loft_emit(&e->w.obj, "move-cursor", &new_pos) == 1)
      return 0;

    e->position = new_pos;
    e->line_offset = e->position - cl->offset;

    _editable_update_cursor(e);
    loft_emit(&e->w.obj, "set-poi", &e->_cursor_pos);

    _editable_bump_cursor(e);
    loft_widget_refresh(&e->w, false);

    return 1;
  }
  else if(k->sym == XK_Right) {
    _editable_clear_selection(e);

    if(e->position == e->max_position)
      return 0;

    LoftBufferLine *cl = loft_buffer_line_from_offset(e->buffer, e->position);
    if(cl == NULL)
      return 0;

    int new_pos = e->position + 1;
    if(loft_emit(&e->w.obj, "move-cursor", &new_pos) == 1)
      return 0;

    e->position = new_pos;
    e->line_offset = e->position - cl->offset;

    _editable_update_cursor(e);
    loft_emit(&e->w.obj, "set-poi", &e->_cursor_pos);

    _editable_bump_cursor(e);
    loft_widget_refresh(&e->w, false);

    return 1;
  }

  if(k->sym == XK_Return) {
    if(loft_emit(&e->w.obj, "return", NULL) == 1)
      return 0;

    k->str[0] = '\n';
  }

  if(k->printable == false)
    return 0;

  e->_buffer_on_changed->active = false;

  if(e->has_selection) {
    int start = utf8_index_to_byte(e->buffer->data, e->selection.start);
    int end = utf8_index_to_byte(e->buffer->data, e->selection.end);

    e->selection = (struct range) { -1,-1 };
    loft_buffer_delete(e->buffer, start, end);
  }

  int byte_pos = utf8_index_to_byte(e->buffer->data, e->position);

  ++e->position;
  ++e->max_position;

  loft_buffer_insert(e->buffer, byte_pos, k->str, -1);
  _editable_update_layout(e);

  e->_buffer_on_changed->active = true;

  _editable_update_cursor(e);
  loft_emit(&e->w.obj, "set-poi", &e->_cursor_pos);

  _editable_bump_cursor(e);
  loft_widget_refresh(&e->w, true);

  return 0;
}

int _editable_on_resize (LoftEditable *e) {
  _editable_update_cursor(e);
  if(e->w.has_input_focus)
    loft_emit(&e->w.obj, "set-poi", &e->_cursor_pos);

  return 0;
}

int _editable_on_set_input_focus (LoftEditable *e, bool *has_input_focus, void *data) {
  if(*has_input_focus) {
    e->_draw_cursor = true;
    loft_timer_start(&e->_cursor_blinker);
  }
  else {
    e->_draw_cursor = false;
    loft_timer_stop(&e->_cursor_blinker);
  }

  loft_widget_refresh_fast(&e->w);
  return 0;
}

int _editable_on_size_request (LoftEditable *e, struct wh *size_req, void *data) {
  int p2 = e->padding * 2;
  int min_w = p2,
      min_h = p2;

  double width;
  if(size_req->width <= 0)
    width = size_req->width;
  else if(size_req->width > p2)
    width = (size_req->width - p2) * PANGO_SCALE;
  else
    width = 0;

  int tw, th;
  pango_layout_set_width(e->_lt, width);
  pango_layout_get_pixel_size(e->_lt, &tw, &th);

  min_w += tw;
  min_h += th;

  loft_widget_set_min_size(&e->w, min_w, min_h);
  return 0;
}

int _editable_buffer_on_changed (LoftBuffer *b, void *arg, LoftEditable *e) {
  if(b->data != NULL)
    e->max_position = utf8_strlen(b->data, NULL);
  else
    e->max_position = 0;

  if(e->position > e->max_position)
    e->position = e->max_position;

  if(loft.state == 1) {
    _editable_update_layout(e);
    _editable_update_cursor(e);

    if(e->w.has_focus)
      _editable_bump_cursor(e);

    loft_widget_refresh(&e->w, true);
  }

  return 0;
}

int _editable_tt_on_changed (LoftTextTagTable *tt, LoftTextTagData *td, LoftEditable *e) {
  _editable_update_layout(e);
  loft_widget_refresh(&e->w, true);
  return 0;
}

int _editable_cursor_blinker_on_timeout (LoftTimer *t, LoftEditable *e) {
  e->_draw_cursor ^= true;
  loft_widget_refresh_fast(&e->w);
  return 0;
}

// --

void loft_editable_clear_selection (LoftEditable *e) {
  e->selection = (struct range) { -1,-1 };
  _editable_update_layout(e);
  loft_widget_refresh(&e->w, false);
}

LoftBuffer *loft_editable_get_buffer (LoftEditable *e) {
  return e->buffer;
}

bool loft_editable_get_line_wrap (LoftEditable *e) {
  return e->line_wrap;
}

int loft_editable_get_padding (LoftEditable *e) {
  return e->padding;
}

char *loft_editable_get_pretext (LoftEditable *e) {
  return e->pretext;
}

struct range loft_editable_get_selection (LoftEditable *e) {
  return e->selection;
}

LoftTextTagTable *loft_editable_get_tag_table (LoftEditable *e) {
  return e->tag_table;
}

bool loft_editable_has_selection (LoftEditable *e) {
  return e->has_selection;
}

void loft_editable_init (LoftEditable *e, LoftBuffer *buffer, char *pretext) {
  loft_widget_init(&e->w, EDITABLE, true, true, SIZE_STATIC);
  e->w.can_focus = true;

  loft_connect(&e->w.obj, "button-press", _editable_on_button_press, NULL);
  loft_connect(&e->w.obj, "deinit", _editable_on_deinit, NULL);
  loft_connect(&e->w.obj, "drag-motion", _editable_on_drag_motion, NULL);
  loft_connect(&e->w.obj, "draw", _editable_on_draw, NULL);
  loft_connect(&e->w.obj, "key-press", _editable_on_key_press, NULL);
  loft_connect(&e->w.obj, "resize", _editable_on_resize, NULL);
  loft_connect(&e->w.obj, "set-input-focus", _editable_on_set_input_focus, NULL);
  loft_connect(&e->w.obj, "size-request", _editable_on_size_request, NULL);

  e->buffer = NULL;
  e->tag_table = NULL;

  e->line_wrap = false;
  e->padding = 4;
  e->pretext = pretext;

  e->cursor_blink_timeout = 500;
  rgba_copy(&e->cursor_color, &loft.colors.common[ACTIVE][IDLE].fg);

  e->_cursor_pos = (struct xywh) { 0,0,0,0 };
  e->_draw_cursor = false;

  loft_timer_init (
      &e->_cursor_blinker,
      e->cursor_blink_timeout,
      _editable_cursor_blinker_on_timeout,
      e
      );

  e->_buffer_on_changed = NULL;
  e->_tt_on_changed = NULL;

  cairo_t *cr = cairo_create(e->w.buffer);
  e->_lt = pango_cairo_create_layout(cr);
  cairo_destroy(cr);

  e->_attrs_unused = NULL;

  loft_text_tag_init(&e->_selection_tag, ATTR_BACKGROUND|ATTR_FOREGROUND);
  rgba_copy(&e->_selection_tag.background, &loft.colors.common[ACTIVE][IDLE].bg);
  rgba_copy(&e->_selection_tag.foreground, &loft.colors.common[ACTIVE][IDLE].fg);

  pango_layout_set_auto_dir(e->_lt, true);
  pango_layout_set_font_description(e->_lt, loft.font.desc);
  pango_layout_set_wrap(e->_lt, PANGO_WRAP_WORD_CHAR);

  loft_editable_set_buffer(e, buffer);
}

void loft_editable_set_buffer (LoftEditable *e, LoftBuffer *b) {
  if(e->buffer != NULL)
    loft_disconnect(&e->buffer->obj, e->_buffer_on_changed);

  if(b == NULL) {
    e->buffer = NULL;
    e->position = 0;
    e->line_offset = 0;
    e->max_position = 0;

    e->_buffer_on_changed = NULL;
  }
  else {
    e->buffer = b;
    e->position = e->max_position;
    e->line_offset = -1;

    if(b->data != NULL)
      e->max_position = utf8_strlen(b->data, NULL);
    else
      e->max_position = 0;

    e->_buffer_on_changed = loft_connect(&b->obj, "changed", _editable_buffer_on_changed, e);
  }

  e->has_selection = false;
  e->selection = (struct range) { -1,-1 };

  _editable_update_layout(e);
  _editable_update_cursor(e);

  loft_widget_refresh(&e->w, true);
}

void loft_editable_set_cursor_blink_timeout (LoftEditable *e, int cursor_blink_timeout) {
  e->cursor_blink_timeout = cursor_blink_timeout;
  loft_timer_set_timeout(&e->_cursor_blinker, e->cursor_blink_timeout);
}

void loft_editable_set_cursor_position (LoftEditable *e, int position) {
  if(position < 0 || position > e->max_position)
    position = e->max_position;

  e->position = position;

  _editable_update_cursor(e);
  loft_emit(&e->w.obj, "set-poi", &e->_cursor_pos);

  _editable_bump_cursor(e);
  loft_widget_refresh(&e->w, false);
}

void loft_editable_set_line_wrap (LoftEditable *e, bool line_wrap) {
  e->line_wrap = line_wrap;
  e->w.size_mode = e->line_wrap ? SIZE_H4W : SIZE_STATIC;
  loft_widget_refresh(&e->w, true);
}

void loft_editable_set_padding (LoftEditable *e, int padding) {
  e->padding = padding;
  loft_widget_refresh(&e->w, true);
}

void loft_editable_set_pretext (LoftEditable *e, char *pretext) {
  e->pretext = pretext;
  if(_editable_update_layout(e) == 1)
    loft_widget_refresh(&e->w, true);
}

void loft_editable_set_selection (LoftEditable *e, int start, int end) {
  e->selection = (struct range) { start, end };
  e->position = end + 1;
  _editable_update_attrs(e);
  _editable_update_cursor(e);
  loft_widget_refresh(&e->w, false);
}

void loft_editable_set_tag_table (LoftEditable *e, LoftTextTagTable *tt) {
  if(e->tag_table != NULL)
    loft_disconnect(&e->tag_table->obj, e->_tt_on_changed);

  e->tag_table = tt;

  if(e->tag_table != NULL) {
    e->_tt_on_changed = loft_connect (
        &e->tag_table->obj, "changed",
        _editable_tt_on_changed, e
        );
  }
  else {
    e->_tt_on_changed = NULL;
  }

  _editable_update_attrs(e);
  loft_widget_refresh(&e->w, false);
}
