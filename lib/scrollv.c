#include "loft.h"

#include <clip/range.h>

char *SCROLLV = "loft-scrollv";

int SCROLL_X = 1;
int SCROLL_Y = 2;
int SCROLL_BI = 3;

void _scrollv_update_clip_adj (LoftScrollView *sv) {
  int border = sv->border_size * 2;

  sv->w.clip_adj.x = sv->border_size;
  sv->w.clip_adj.y = sv->border_size;

  if(sv->policy & SCROLL_Y)
    sv->w.clip_adj.width = border + sv->slider_size;
  else
    sv->w.clip_adj.width = border;

  if(sv->policy & SCROLL_X)
    sv->w.clip_adj.height = border + sv->slider_size;
  else
    sv->w.clip_adj.height = border;

  sv->w.dirty_clip = true;
}

void _scrollv_update_span (LoftScrollView *sv) {
  int border = sv->border_size * 2;

  if(sv->policy & SCROLL_X)
    sv->span.height = sv->w.height - border - sv->slider_size;
  else
    sv->span.height = sv->w.height - border;

  if(sv->policy & SCROLL_Y)
    sv->span.width = sv->w.width - border - sv->slider_size;
  else
    sv->span.width = sv->w.width - border;
}

void _scrollv_update_sl_geo (LoftScrollView *sv) {
  double total;

  if(sv->policy & SCROLL_X) {
    sv->x_sl_geo.y = sv->w.y + (sv->w.height - sv->border_size - sv->slider_size);
    sv->x_sl_geo.height = sv->slider_size;

    total = loft_adj_get_max(&sv->x_adj) + sv->span.width;

    if(sv->w.child != NULL && sv->w.child->visible && total > sv->span.width) {
      double xo = loft_adj_get_value_bounded(&sv->x_adj);
      double xp = range_to_perc(xo, 0, sv->w.child->width);
      double xr = perc_to_range(xp, 0, sv->span.width);

      sv->x_sl_geo.x = sv->w.x + sv->border_size + xr;
      sv->x_sl_geo.width = sv->span.width / (total / sv->span.width);
    }
    else {
      sv->x_sl_geo.x = sv->w.x + sv->border_size;
      sv->x_sl_geo.width = sv->span.width;
    }
  }

  if(sv->policy & SCROLL_Y) {
    sv->y_sl_geo.x = sv->w.x + (sv->w.width - sv->border_size - sv->slider_size);
    sv->y_sl_geo.width = sv->slider_size;

    total = loft_adj_get_max(&sv->y_adj) + sv->span.height;

    if(sv->w.child != NULL && sv->w.child->visible && total > sv->span.height) {
      double yo = loft_adj_get_value_bounded(&sv->y_adj);
      double yp = range_to_perc(yo, 0, sv->w.child->height);
      double yr = perc_to_range(yp, 0, sv->span.height);

      sv->y_sl_geo.y = sv->w.y + sv->border_size + yr;
      sv->y_sl_geo.height = sv->span.height / (total / sv->span.height);
    }
    else {
      sv->y_sl_geo.y = sv->w.y + sv->border_size;
      sv->y_sl_geo.height = sv->span.height;
    }
  }
}

bool _scrollv_update_sl_prelight (LoftScrollView *sv) {
  bool ret = false;

  if(loft_xy_within(&sv->pointer, &sv->y_sl_geo)) {
    if(sv->y_sl_prelight == false) {
      sv->y_sl_prelight = true;
      return true;
    }
  }
  else if(sv->y_sl_prelight) {
    sv->y_sl_prelight = false;
    ret = true;
  }

  if(loft_xy_within(&sv->pointer, &sv->x_sl_geo)) {
    if(sv->x_sl_prelight == false) {
      sv->x_sl_prelight = true;
      return true;
    }
  }
  else if(sv->x_sl_prelight) {
    sv->x_sl_prelight = false;
    ret = true;
  }

  return ret;
}

// child signal callbacks - predeclared for _scrollv_on_add_child()

int _scrollv_child_on_resize (LoftWidget *c, void *arg, LoftScrollView *sv) {
  _scrollv_update_span(sv);
  _scrollv_update_sl_geo(sv);
  _scrollv_update_sl_prelight(sv);
  sv->w.dirty = true;
  return 0;
}

int _scrollv_child_on_set_focus (LoftWidget *c, void *arg, LoftScrollView *sv) {
  if(sv->border_size > 0)
    sv->w.dirty = true;
  return 0;
}

int _scrollv_child_on_set_poi (LoftWidget *c, struct xywh *poi, LoftScrollView *sv) {
  int ret = 0;

  if(sv->policy & SCROLL_X) {
    int xo = loft_adj_get_value_bounded(&sv->x_adj);
    if(poi->x < xo) {
      loft_adj_set_value(&sv->x_adj, poi->x);
      ret = 1;
    }
    else {
      int xr = poi->x + poi->width;
      if(xr > xo + sv->span.width) {
        loft_adj_set_value(&sv->x_adj, xr - sv->span.width);
        ret = 1;
      }
    }
  }

  if(sv->policy & SCROLL_Y) {
    int yo = loft_adj_get_value_bounded(&sv->y_adj);
    if(poi->y < yo) {
      loft_adj_set_value(&sv->y_adj, poi->y);
      ret = 1;
    }
    else {
      int yb = poi->y + poi->height;
      if(yb > yo + sv->span.height) {
        loft_adj_set_value(&sv->y_adj, yb - sv->span.height);
        ret = 1;
      }
    }
  }

  if(ret == 1) {
    sv->w.dirty = true;

    //  In case the child's POI was updated in response to a resize
    //  that was triggered by an arrangement, the scrollview needs
    //  to propagate the new offset by arranging itself yet again.

    loft_emit(&sv->w.obj, "arrange", NULL);
  }

  return ret;
}

// --

int _scrollv_adj_on_max_changed (LoftAdjustment *adj, void *arg, LoftScrollView *sv) {
  _scrollv_update_sl_geo(sv);
  _scrollv_update_sl_prelight(sv);
  return 0;
}

int _scrollv_adj_on_value_changed (LoftAdjustment *adj, void *arg, LoftScrollView *sv) {
  _scrollv_update_sl_geo(sv);
  _scrollv_update_sl_prelight(sv);
  loft_widget_refresh_fast(&sv->w);
  return 0;
}

int _scrollv_on_arrange (LoftScrollView *sv, void *arg, void *data) {
  LoftWidget *c = sv->w.child;
  if(sv->w.child == NULL) {
    loft_adj_set_max(&sv->x_adj, 0);
    loft_adj_set_max(&sv->y_adj, 0);
    return 0;
  }

  int base_w, base_h;
  loft_widget_base_size(c, &base_w, &base_h);

  int cw;
  bool expand_x = (c->attrs & (EXPAND_X|PACK_X)) == EXPAND_X;
  if(expand_x && (base_w < sv->span.width || (sv->policy & SCROLL_X) == 0))
    cw = sv->span.width;
  else
    cw = base_w;

  int ch;
  bool expand_y = (c->attrs & (EXPAND_Y|PACK_Y)) == EXPAND_Y;
  if(expand_y && (base_h < sv->span.height || (sv->policy & SCROLL_Y) == 0))
    ch = sv->span.height;
  else
    ch = base_h;

  loft_adj_set_max(&sv->x_adj, base_w > sv->span.width ? base_w - sv->span.width : 0);
  loft_adj_set_max(&sv->y_adj, base_h > sv->span.height ? base_h - sv->span.height : 0);

  int cx = (sv->w.x + sv->border_size) - loft_adj_get_value_bounded(&sv->x_adj);
  int cy = (sv->w.y + sv->border_size) - loft_adj_get_value_bounded(&sv->y_adj);

  loft_widget_configure(c, cx, cy, cw, ch);
  return 0;
}

int _scrollv_on_button_press (LoftScrollView *sv, LoftButtonEvent *be, void *data) {
  if(be->state & ControlMask) {
    if(be->button == 4) {
      loft_scrollv_scroll_x(sv, sv->step * -1);
      return 0;
    }
    else if(be->button == 5) {
      loft_scrollv_scroll_x(sv, sv->step);
      return 0;
    }
  }
  else {
    if(be->button == 4) {
      loft_scrollv_scroll_y(sv, sv->step * -1);
      return 0;
    }
    else if(be->button == 5) {
      loft_scrollv_scroll_y(sv, sv->step);
      return 0;
    }
  }

  if(sv->y_sl_prelight) {
    sv->drag_offset = be->y - (sv->y_sl_geo.y + (sv->y_sl_geo.height / 2));
    sv->drag_orientation = SCROLL_Y;
    return 2;
  }
  else if(sv->x_sl_prelight) {
    sv->drag_offset = be->x - (sv->x_sl_geo.x + (sv->x_sl_geo.width / 2));
    sv->drag_orientation = SCROLL_X;
    return 2;
  }

  return 0;
}

int _scrollv_on_configure (LoftScrollView *sv, void *arg, void *data) {
  _scrollv_update_sl_geo(sv);
  return 0;
}

int _scrollv_on_destroy (LoftScrollView *sv, void *arg, void *data) {
  loft_object_destroy(&sv->x_adj.obj);
  loft_object_destroy(&sv->y_adj.obj);
  return 0;
}

int _scrollv_on_drag_motion (LoftScrollView *sv, LoftPointerEvent *pe, void *data) {
  sv->pointer = pe->p;

  double perc;
  if(sv->drag_orientation == SCROLL_Y) {
    int half_sl_h = sv->y_sl_geo.height / 2;
    perc = range_to_perc(pe->y - sv->w.y - sv->drag_offset, half_sl_h, sv->w.height - half_sl_h);
    loft_adj_set_perc_bounded(&sv->y_adj, perc);
  }
  else { // sv->drag_orientation == SCROLL_X
    int half_sl_w = sv->x_sl_geo.width / 2;
    perc = range_to_perc(pe->x - sv->w.x - sv->drag_offset, half_sl_w, sv->w.width - half_sl_w);
    loft_adj_set_perc_bounded(&sv->x_adj, perc);
  }

  return 0;
}

int _scrollv_on_drag_release (LoftScrollView *sv, LoftPointerEvent *pe, void *data) {
  sv->pointer = pe->p;
  _scrollv_update_sl_prelight(sv);
  loft_widget_refresh_fast(&sv->w);
  return 0;
}

int _scrollv_on_drag_start (LoftScrollView *sv, LoftPointerEvent *pe, void *data) {
  sv->pointer = pe->p;
  loft_widget_refresh_fast(&sv->w);
  return 0;
}

int _scrollv_on_draw (LoftScrollView *sv, cairo_t *cr, void *data) {
  LoftWidget *c = sv->w.child;

  int state, pair;
  struct rgba *sl_x_c;
  struct rgba *sl_y_c;

  if(sv->w.sensitive) {
    state = NORMAL;
    pair = c != NULL && c->has_focus ? SELECTED : IDLE;

    if(sv->w.dragging && sv->drag_orientation == SCROLL_X)
      sl_x_c = &sv->w.colors[state][SELECTED].bg;
    else if(sv->x_sl_prelight)
      sl_x_c = &sv->w.colors[state][PRELIGHT].fg;
    else
      sl_x_c = &sv->w.colors[state][IDLE].fg;

    if(sv->w.dragging && sv->drag_orientation == SCROLL_Y)
      sl_y_c = &sv->w.colors[state][SELECTED].bg;
    else if(sv->y_sl_prelight)
      sl_y_c = &sv->w.colors[state][PRELIGHT].fg;
    else
      sl_y_c = &sv->w.colors[state][IDLE].fg;
  }
  else {
    state = INSENSITIVE;
    pair = IDLE;

    sl_x_c = &sv->w.colors[state][pair].fg;
    sl_y_c = &sv->w.colors[state][pair].fg;
  }

  if(sv->border_size > 0) {
    rgba_cairo_set(cr, &sv->w.colors[state][pair].bg);

    int bi;
    int bx = 0,
        by = 0,
        bw = sv->w.width,
        bh = sv->w.height;

    cairo_set_line_width(cr, 1.0);

    for(bi = 0; bi < sv->border_size; ++bi) {
      loft_cairo_solid_rectangle(cr, bx, by, bw, bh);
      cairo_stroke(cr);

      ++bx; ++by;
      bw -= 2; bh -= 2;
    }
  }

  if(sv->policy & SCROLL_X) {
    double y = sv->w.height - sv->border_size - sv->slider_size;

    rgba_cairo_set(cr, &sv->w.colors[state][IDLE].bg);
    cairo_rectangle(cr, sv->border_size, y, sv->span.width, sv->slider_size);
    cairo_fill(cr);

    rgba_cairo_set(cr, sl_x_c);
    cairo_rectangle(cr, sv->x_sl_geo.x - sv->w.x, y, sv->x_sl_geo.width, sv->slider_size);
    cairo_fill(cr);
  }

  if(sv->policy & SCROLL_Y) {
    double x = sv->w.width - sv->border_size - sv->slider_size;

    rgba_cairo_set(cr, &sv->w.colors[state][IDLE].bg);
    cairo_rectangle(cr, x, sv->border_size, sv->slider_size, sv->span.height);
    cairo_fill(cr);

    rgba_cairo_set(cr, sl_y_c);
    cairo_rectangle(cr, x, sv->y_sl_geo.y - sv->w.y, sv->slider_size, sv->y_sl_geo.height);
    cairo_fill(cr);
  }

  return 0;
}

int _scrollv_on_pointer_leave (LoftScrollView *sv, LoftPointerEvent *pe, void *data) {
  sv->x_sl_prelight = false;
  sv->y_sl_prelight = false;
  loft_widget_refresh_fast(&sv->w);
  return 0;
}

int _scrollv_on_pointer_motion (LoftScrollView *sv, LoftPointerEvent *pe, void *data) {
  sv->pointer = pe->p;
  if(_scrollv_update_sl_prelight(sv))
    loft_widget_refresh_fast(&sv->w);
  return 0;
}

int _scrollv_on_pre_add_child (LoftScrollView *sv, LoftWidget *c, void *data) {
  if(sv->w.child != NULL)
    loft_widget_detach(sv->w.child);

  sv->_child_resize = loft_connect(&c->obj, "resize", _scrollv_child_on_resize, sv);
  sv->_child_set_focus = loft_connect(&c->obj, "set-focus", _scrollv_child_on_set_focus, sv);
  sv->_child_set_poi = loft_connect(&c->obj, "set-poi", _scrollv_child_on_set_poi, sv);

  return 0;
}

int _scrollv_on_resize (LoftScrollView *sv, void *arg, void *data) {
  _scrollv_update_clip_adj(sv);
  _scrollv_update_span(sv);
  _scrollv_update_sl_prelight(sv);
  return 0;
}

int _scrollv_on_rm_child (LoftScrollView *sv, LoftWidget *c, void *data) {
  loft_adj_set_max(&sv->x_adj, 0);
  loft_adj_set_max(&sv->y_adj, 0);

  loft_disconnect(&c->obj, sv->_child_resize);
  loft_disconnect(&c->obj, sv->_child_set_focus);
  loft_disconnect(&c->obj, sv->_child_set_poi);

  sv->_child_resize = NULL;
  sv->_child_set_focus = NULL;
  sv->_child_set_poi = NULL;

  _scrollv_update_span(sv);
  _scrollv_update_sl_geo(sv);

  return 0;
}

int _scrollv_on_size_request (LoftScrollView *sv, struct wh *size_req, void *data) {
  LoftWidget *w = &sv->w;
  LoftWidget *c = w->child;

  int b2 = sv->border_size * 2;
  int min_w = b2,
      min_h = b2;

  if(sv->policy & SCROLL_Y)
    min_w += sv->slider_size;

  if(sv->policy & SCROLL_X)
    min_h += sv->slider_size;

  if(c != NULL && c->visible && sv->policy != SCROLL_BI) {
    struct wh c_size_req = { .height = -1 };
    if(c->size_mode == SIZE_H4W) {
      if(size_req->width > min_w)
        c_size_req.width = size_req->width - min_w;
      else
        c_size_req.width = 0;
    }
    else {
      c_size_req.width = -1;
    }

    loft_emit(&c->obj, "size-request", &c_size_req);

    int base_w, base_h;
    loft_widget_base_size(c, &base_w, &base_h);

    if((sv->policy & SCROLL_X) == 0)
      min_w += base_w;

    if((sv->policy & SCROLL_Y) == 0)
      min_h += base_h;
  }

  loft_widget_set_min_size(w, min_w, min_h);
  return 0;
}

// --

int loft_scrollv_get_step (LoftScrollView *sv) {
  return sv->step;
}

int loft_scrollv_get_x_offset (LoftScrollView *sv) {
  return loft_adj_get_value_bounded(&sv->x_adj);
}

int loft_scrollv_get_y_offset (LoftScrollView *sv) {
  return loft_adj_get_value_bounded(&sv->y_adj);
}

void loft_scrollv_init (LoftScrollView *sv, int policy) {
  loft_widget_init(&sv->w, SCROLLV, true, true, SIZE_H4W);
  sv->w.can_clip = true;

  loft_connect(&sv->w.obj, "arrange", _scrollv_on_arrange, NULL);
  loft_connect(&sv->w.obj, "button-press", _scrollv_on_button_press, NULL);
  loft_connect(&sv->w.obj, "configure", _scrollv_on_configure, NULL);
  loft_connect(&sv->w.obj, "destroy", _scrollv_on_destroy, NULL);
  loft_connect(&sv->w.obj, "drag-motion", _scrollv_on_drag_motion, NULL);
  loft_connect(&sv->w.obj, "drag-release", _scrollv_on_drag_release, NULL);
  loft_connect(&sv->w.obj, "drag-start", _scrollv_on_drag_start, NULL);
  loft_connect(&sv->w.obj, "draw", _scrollv_on_draw, NULL);
  loft_connect(&sv->w.obj, "pointer-leave", _scrollv_on_pointer_leave, NULL);
  loft_connect(&sv->w.obj, "pointer-motion", _scrollv_on_pointer_motion, NULL);
  loft_connect(&sv->w.obj, "pre-add-child", _scrollv_on_pre_add_child, NULL);
  loft_connect(&sv->w.obj, "resize", _scrollv_on_resize, NULL);
  loft_connect(&sv->w.obj, "rm-child", _scrollv_on_rm_child, NULL);
  loft_connect(&sv->w.obj, "size-request", _scrollv_on_size_request, NULL);

  sv->border_size = 1;
  sv->policy = policy;
  sv->slider_size = 8;
  sv->step = 8;

  loft_adj_init(&sv->x_adj);
  loft_adj_init(&sv->y_adj);

  sv->span = sv->w.size;
  sv->x_sl_prelight = false;
  sv->y_sl_prelight = false;

  loft_connect(&sv->x_adj.obj, "max-changed", _scrollv_adj_on_max_changed, sv);
  loft_connect(&sv->x_adj.obj, "value-changed", _scrollv_adj_on_value_changed, sv);

  loft_connect(&sv->y_adj.obj, "max-changed", _scrollv_adj_on_max_changed, sv);
  loft_connect(&sv->y_adj.obj, "value-changed", _scrollv_adj_on_value_changed, sv);

  sv->_child_resize = NULL;
  sv->_child_set_focus = NULL;
  sv->_child_set_poi = NULL;

  _scrollv_update_clip_adj(sv);
  _scrollv_update_span(sv);
  _scrollv_update_sl_geo(sv);
}

void loft_scrollv_scroll_x (LoftScrollView *sv, int amount) {
  if(amount == 0 || (sv->policy & SCROLL_X) == 0)
    return;

  int xo = loft_adj_get_value_bounded(&sv->x_adj);
  int new_xo = xo + amount;

  if(new_xo != xo)
    loft_scrollv_set_x_offset(sv, new_xo);
}

void loft_scrollv_scroll_y (LoftScrollView *sv, int amount) {
  if(amount == 0 || (sv->policy & SCROLL_Y) == 0)
    return;

  int yo = loft_adj_get_value_bounded(&sv->y_adj);
  int new_yo = yo + amount;

  if(new_yo != yo)
    loft_scrollv_set_y_offset(sv, new_yo);
}

void loft_scrollv_set_border_size (LoftScrollView *sv, int border_size) {
  sv->border_size = border_size;

  _scrollv_update_span(sv);
  _scrollv_update_clip_adj(sv);
  _scrollv_update_sl_geo(sv);

  loft_widget_refresh(&sv->w, true);
}

void loft_scrollv_set_policy (LoftScrollView *sv, int policy) {
  sv->policy = policy;

  _scrollv_update_clip_adj(sv);
  _scrollv_update_span(sv);
  _scrollv_update_sl_geo(sv);

  loft_widget_refresh(&sv->w, true);
}

void loft_scrollv_set_slider_size (LoftScrollView *sv, int slider_size) {
  sv->slider_size = slider_size;

  _scrollv_update_span(sv);
  _scrollv_update_clip_adj(sv);
  _scrollv_update_sl_geo(sv);
  _scrollv_update_sl_prelight(sv);

  loft_widget_refresh(&sv->w, true);
}

void loft_scrollv_set_step (LoftScrollView *sv, int step) {
  sv->step = step;
  loft_emit(&sv->w.obj, "set-step", &sv->step);
}

void loft_scrollv_set_x_offset (LoftScrollView *sv, int x_offset) {
  loft_adj_set_value_bounded(&sv->x_adj, x_offset);
}

void loft_scrollv_set_y_offset (LoftScrollView *sv, int y_offset) {
  loft_adj_set_value_bounded(&sv->y_adj, y_offset);
}
