#include <loft.h>

LoftWindow win;
LoftLayout lt_main;
LoftScrollView sv;
LoftBuffer buf;
LoftEditable e;
LoftLayout lt_status;
LoftLabel lb_mode;
LoftLabel lb_info;

int _scrollv_on_button_press (LoftScrollView* sv, LoftButtonEvent* be, void* data) {
    if (be->button == 4) {
        loft_scrollv_scroll_y(sv, -8);
        return 1;
    }
    else if (be->button == 5) {
        loft_scrollv_scroll_y(sv, +8);
        return 1;
    }

    return 0;
}

int _window_on_delete (LoftWindow* win, void* arg, void* data) { 
    loft_break();
    return 1;
}

int _window_on_key_press (LoftWindow* win, LoftKey* k, void* data) {
    if ((k->state & ControlMask) && k->sym == XK_q) {
        loft_break();
        return 1;
    }

    return 0;
}

int main (int argc, char** argv) {
    loft_init();

    loft_window_init(&win, "Demo Application", 5, false);
    loft_layout_init(&lt_main, ASPECT_V, 5);
    loft_scrollv_init(&sv, SCROLL_Y);
    loft_buffer_init(&buf, NULL, -1);
    loft_editable_init(&e, &buf, NULL);
    loft_layout_init(&lt_status, ASPECT_H, 0);
    loft_label_init(&lb_mode, "-- <b>INSERT</b> --", true);
    loft_label_init(&lb_info, "This is some useful info.", false);

    loft_editable_set_line_wrap(&e, true);
    loft_label_set_highlighted(&lb_mode, true);

    loft_connect(&sv.w.obj, "button-press", _scrollv_on_button_press, NULL);
    loft_connect(&win.w.obj, "delete", _window_on_delete, NULL);
    loft_connect(&win.w.obj, "key-press", _window_on_key_press, NULL);

    loft_widget_set_attrs(&lt_main.w, EXPAND);
    loft_widget_set_attrs(&lt_status.w, EXPAND_X);
    loft_widget_set_attrs(&sv.w, EXPAND);
    loft_widget_set_attrs(&e.w, EXPAND);
    loft_widget_set_attrs(&lb_info.w, EXPAND_X|PACK_X|FLOW_R);

    loft_widget_attach(&lt_main.w, &win.w, -1);
    loft_widget_attach(&e.w, &sv.w, -1);
    loft_widget_attach(&sv.w, &lt_main.w, -1);
    loft_widget_attach(&lt_status.w, &lt_main.w, -1);
    loft_widget_attach(&lb_mode.w, &lt_status.w, -1);
    loft_widget_attach(&lb_info.w, &lt_status.w, -1);

    loft_widget_set_pref_size(&win.w, 300, 250);
    loft_window_show_all(&win);

    loft_main(NULL);
}
